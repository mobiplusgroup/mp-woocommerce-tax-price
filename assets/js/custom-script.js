jQuery(document).ready(function () {
	jQuery('input[type=radio][name=mp-woocommerce-price-option]').change(function() {
		this.form.submit();
	});
	
	jQuery('.mopiplus_tax_button').on('click', function(event){
		jQuery("#mp-woocommerce-price-option").val(this.value);
	});
	
	if ( jQuery("#mp_display_tax_price_form").find(".mopiplus_tax_button")) {
		var activeButton 	= jQuery("#mp-woocommerce-price-option").val();
		jQuery('.'+activeButton+'_active').prop('disabled', true);
	}
});