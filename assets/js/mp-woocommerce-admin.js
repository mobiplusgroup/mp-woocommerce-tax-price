jQuery(document).ready(function () {
	jQuery('#mp_tax_display_type').change(function() {
		mp_display_type();
	});
	mp_display_type();
	jQuery("#mp_tax_default").after( "<label><b>"+jQuery("#mp_tax_default").attr("placeholder")+"</b></label>" );
	jQuery("#mp_user_role_selected").after( "<label><b>"+jQuery("#mp_user_role_selected").attr("placeholder")+"</b></label>" );
});

function mp_display_type() {
	var displayType	= jQuery("#mp_tax_display_type").val();
	jQuery(".hidden_for_radio").parent().parent().show();
	if (displayType == "radio") jQuery(".hidden_for_radio").parent().parent().hide();
}