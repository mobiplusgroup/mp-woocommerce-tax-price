<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Mobiplus_Settings' ) ) :

class Mobiplus_Settings extends WC_Settings_Page {

	function __construct() {
		
		if ( is_admin() ) {
			$this->id    = 'mp-price-display';
			$this->label = __( 'Mobiplus Price Display', 'mp-woocommerce-price' );
	
			add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 30 );
			add_action( 'woocommerce_settings_' . $this->id,       array( $this, 'output' ) );
			add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
		}
	}
	
	public function get_settings() {
	
		$shop_tax			= get_option( "woocommerce_tax_display_shop", '' );
		$shop_tax_options 	= array(
								'incl'       => __( 'Including tax', 'mp-woocommerce-price' ),
								'excl'       => __( 'Excluding tax', 'mp-woocommerce-price' ),
							);
		$user_shop_tax		= ($shop_tax=="incl")?"excl":"incl";
		
		$settings =  array(
						array(
							'title' => __( 'Tax Settings', 'mp-woocommerce-price' ),
							'type'  => 'title',
							'desc'     => sprintf( __( 'Use %s shortcode to display the button on frontend.', 'mp-woocommerce-price' ), '<code>[mobiplus_tax_display]</code>' ),
							'id'    => 'tax-settings',
						),
					
						array(
							'title'    => __( 'Use selector', 'mp-woocommerce-price' ),
							'id'       => 'mopiplus_tax_display',
							'default'  => 'yes',
							'type'     => 'checkbox'
						),
							
						array(
							'title'    => __( 'Default', 'mp-woocommerce-price' ),
							'id'       => 'mp_tax_default',
							'type'     => 'text',
							'css'	   => 'display:none',
							'placeholder'     => $shop_tax_options[$shop_tax],
						),
						
						array(
							'title'    => __( 'Display Type ', 'mp-woocommerce-price' ),
							'id'       => 'mp_tax_display_type',
							'default'  => "radio",
							'type'     => 'select',
							'options'  => array(
								'radio'       => __( 'Radio', 'mp-woocommerce-price' ),
								'button'       => __( 'Button', 'mp-woocommerce-price' ),
							),
						),
							
						array(
							'title'    => __( 'Button Class Name', 'mp-woocommerce-price' ),
							'id'       => 'mp_button_class_name',
							'class'	   =>  'hidden_for_radio',
							'type'     => 'text',
						),	
						
						array(
							'title'    => __( 'Pre Text', 'mp-woocommerce-price' ),
							'id'       => 'mp_pre_text',
							'default'  => __( 'Show Prices', 'mp-woocommerce-price' ),
							'type'     => 'text',
						),	
												
						array(
							'title'    => __( 'Including TAX Label', 'mp-woocommerce-price' ),
							'id'       => 'mp_inc_tax_label',
							'default'  => __( 'Including TAX', 'mp-woocommerce-price' ),
							'type'     => 'text',
						),
						
										
						array(
							'title'    => __( 'Including TAX Label Class Name', 'mp-woocommerce-price' ),
							'id'       => 'mp_inc_tax_label_class_name',
							'type'     => 'text',
						),	
						
						array(
							'title'    => __( 'Excluding TAX Label', 'mp-woocommerce-price' ),
							'id'       => 'mp_exc_tax_label',
							'default'  => __( 'Excluding TAX', 'mp-woocommerce-price' ),
							'type'     => 'text',
						),
				
						array(
							'title'    => __( 'Excluding TAX Label Class Name', 'mp-woocommerce-price' ),
							'id'       => 'mp_exc_tax_label_class_name',
							'type'     => 'text',
						),					

						array(
							'type' => 'sectionend',
							'id'   => 'tax-settings',
						),
							
						array(
							'title'    => __( 'TAX Display by User Role', 'mp-woocommerce-price' ),
							'type'     => 'title',
							'desc'     => __( 'If you want to display prices including TAX or excluding TAX for different user roles, you can set it here.', 'mp-woocommerce-price' ),
							'id'       => 'mp_tax_display_title',
						),
						
						array(
							'title'    => __( 'TAX Display by User Role', 'mp-woocommerce-price' ),
							'id'       => 'mp_tax_display_by_user_role',
							'type'     => 'checkbox',
							'default'  => 'no',
						),
						
						array(
							'title'    => __( 'User Roles', 'mp-woocommerce-price' ),
							'id'       => 'mp_user_role_roles',
							'type'     => 'multiselect',
							'class'    => 'chosen_select',
							'default'  => '',
							'options'  => $this->mp_user_roles_options(),
							'desc'		=> __( 'NOTE! Save before settings appear', 'mp-woocommerce-price' )
					),
					
						array(
							'title'    => sprintf( __( 'Role for Selected User Roles', 'mp-woocommerce-price' ) ),
							'id'       => 'mp_user_role_selected',
							'type'     => 'text',
							'css'	   => 'display:none',
							'placeholder'     => $shop_tax_options[$user_shop_tax],
					)
				 );
			
			
		
		$settings = array_merge( $settings, array(
			array(
				'type'     => 'sectionend',
				'id'       => 'mp_tax_display_title',
			),
		) );

		return $settings ;
	}
	
	public function output() {
		$settings = $this->get_settings();
		WC_Admin_Settings::output_fields( $settings );
		
		wp_enqueue_script('jquery');
		wp_enqueue_script('script', plugins_url('mp-woocommerce-tax-price/assets/js/mp-woocommerce-admin.js'));
	}

	public function save() {
		$settings = $this->get_settings();
		WC_Admin_Settings::save_fields( $settings );
	}
	
	private function mp_user_roles_options() {
		global $wp_roles;
		$user_roles		= $wp_roles->roles;
		
		$guest_option	= array( 'guest' => array( 'name'         => __( 'Guest', 'mp-woocommerce-price' )) );
		$user_roles 	= array_merge( $guest_option , $user_roles );

		$user_roles_options = array();
		foreach ( $user_roles as $_role_key => $_role ) {
			$user_roles_options[ $_role_key ] = $_role['name'];
		}
		return $user_roles_options;
	}
}
endif;

return new Mobiplus_Settings();