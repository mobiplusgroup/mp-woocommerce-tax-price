<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Mb_Display_Tax_Price' ) ) :

class Mb_Display_Tax_Price {

	var $shop_tax;

	function __construct() {
	
		if ( ! is_admin() ) {
	
			if ( ! session_id() ) {
				session_start();
			}
			
			$this->shop_tax	= get_option( "woocommerce_tax_display_shop", '' );
			add_action( 'plugins_loaded', array( $this,'mp_init'),10 );
			add_action( 'wp_logout', array( $this,'mp_logout'));
	
			add_filter( 'option_woocommerce_tax_display_shop', array( $this, 'mp_tax_display_price' ), PHP_INT_MAX );
			add_filter( 'option_woocommerce_tax_display_cart', array( $this, 'mp_tax_display_price' ), PHP_INT_MAX );
				
			if ( 'yes' === get_option( 'mp_tax_display_by_user_role', 'no' ) ) {
				add_filter( 'option_woocommerce_tax_display_shop', array( $this, 'mp_tax_display_by_user_role' ), PHP_INT_MAX );
				add_filter( 'option_woocommerce_tax_display_cart', array( $this, 'mp_tax_display_by_user_role' ), PHP_INT_MAX );
			} 
			
			add_shortcode( 'mobiplus_tax_display', array( $this, 'mp_display_tax_form' ));
		}
	}
	
	public function mp_logout () {
		unset($_SESSION[ "mp_user_role" ]);
	}
	
	public function mp_init () {
		
		if ( isset( $_POST['mp-woocommerce-price-option'] ) ) {
			$_SESSION[ 'mp-woocommerce-price-option' ] = $_POST['mp-woocommerce-price-option'];
		}
	}
	
	public function mp_tax_display_by_user_role( $value ) {
		
		$exValue	= $value;
		$mp_user_role_roles = get_option( 'mp_user_role_roles', '' );

		$current_user 		= wp_get_current_user();
		
		$user_role   = ( isset( $current_user->roles ) && is_array( $current_user->roles ) && ! empty( $current_user->roles ) ? reset( $current_user->roles ) : 'guest' );	
		
		if (empty($_SESSION[ "mp_user_role" ])) {
			if ( in_array( $user_role, $mp_user_role_roles ) ) {			
				$value		= ($this->shop_tax=="incl")?"excl":"incl";		
				$_SESSION[ "mp_user_role"] = $_SESSION[ 'mp-woocommerce-price-option' ] = $value;
				return $value;
			}
		} 
		
		return $exValue;
	}
	
	public function mp_tax_display_price( $value ) {
		
		if ( 'yes' === get_option( 'mopiplus_tax_display', 'yes' ) ) {
			$value		= isset( $_SESSION[ 'mp-woocommerce-price-option' ] ) ? $_SESSION[ 'mp-woocommerce-price-option' ] : $this->shop_tax;
		}
		return $value;
	}
	
	public function mp_display_tax_form() {
		if ( 'yes' === get_option( 'mopiplus_tax_display', 'yes' ) ) {
			
			$mp_pre_text		= get_option( 'mp_pre_text', esc_html( __('Show Prices', 'mp-woocommerce-price' )) );
			$mp_inc_tax_label	= get_option( 'mp_inc_tax_label', esc_html( __('Including TAX', 'mp-woocommerce-price' )) );
			$mp_exc_tax_label	= get_option( 'mp_exc_tax_label', esc_html( __('Excluding TAX', 'mp-woocommerce-price' )) );
			$mp_tax_default		= !empty( $_SESSION[ 'mp-woocommerce-price-option' ] ) ? $_SESSION[ 'mp-woocommerce-price-option' ] : $this->shop_tax;
			
			$content = '<form id="mp_display_tax_price_form" method="post" action="">';
			$content .= "<div class='mp_display_tax_price'>".(!empty($mp_pre_text)?"<label>".$mp_pre_text."</label>&nbsp;&nbsp;":"");
			
			$mp_inc_tax_label_class_name	= get_option( 'mp_inc_tax_label_class_name', '');
			$mp_exc_tax_label_class_name	= get_option( 'mp_exc_tax_label_class_name', '');
			
			if ( 'radio' === get_option( 'mp_tax_display_type', 'radio' ) ) {
				$content .=	"<input type='radio' value='incl' ".(($mp_tax_default == "incl")?"checked='checked'":"")." name='mp-woocommerce-price-option' id='mp-woocommerce-price-option'><label class='mp_inc_tax_label ".$mp_inc_tax_label_class_name."' for='mp-woocommerce-price-option'>".$mp_inc_tax_label."</label></span>";
				$content .=	"&nbsp;&nbsp;<input type='radio' value='excl' ".(($mp_tax_default == "excl")?"checked='checked'":"")." name='mp-woocommerce-price-option' id='mp-woocommerce-price-option2'><label class='mp_excl_tax_label ".$mp_exc_tax_label_class_name."' for='mp-woocommerce-price-option2'>".$mp_exc_tax_label."</label>";
			} else {
				
				$mp_button_class_name 	= get_option( 'mp_button_class_name', '');
				$content .= "<button type='submit' class='button mopiplus_tax_button incl_active ".$mp_button_class_name."' value='incl'><span class='mp_inc_tax_label ".$mp_inc_tax_label_class_name."'>".$mp_inc_tax_label."</span></button>";
				$content .= "<button type='submit' class='button mopiplus_tax_button excl_active ".$mp_button_class_name."' value='excl'><span class='mp_excl_tax_label ".$mp_exc_tax_label_class_name."'>".$mp_exc_tax_label."</span></button>";
				$content .= "<input type='hidden' name='mp-woocommerce-price-option' id='mp-woocommerce-price-option' value='".$mp_tax_default."'>";
			}
			$content .= "</div></form>";
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('script', plugins_url("mp-woocommerce-tax-price/assets/js/custom-script.js" ));
			
			echo $content;
		}
	}
}
endif;

new Mb_Display_Tax_Price();