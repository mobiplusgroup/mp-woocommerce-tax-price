﻿<?php
/*
Plugin Name: Mobiplus WooCommerce Price Display With Tax or Without Tax
Plugin URI: http://wordpress.org/plugins/mobiplus-woocommerce-price/
Description: Offers User to switch product price to display With Tax or Without Tax
Version: 1.0
Author: Mobiplus
Author URI: http://www.mobiplus.se
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Mobiplus_Woocommerce_Display_Price' ) ) :

class Mobiplus_Woocommerce_Display_Price {
	
	function __construct() {
		add_action( 'admin_menu', array( $this, 'mp_woocommerce_price_tab' ), PHP_INT_MAX );
		add_filter( 'woocommerce_get_settings_pages', array( $this, 'mp_add_settings_tab' ), 1 );
		add_action( 'plugins_loaded', array( $this, 'mp_load_language' ));
		
		require_once( 'includes/class-mobiplus.php' );
     }
	 
	 function mp_add_settings_tab( $settings ) {
		 
		$_settings[] = require_once( 'includes/class-mobiplus-settings.php' );
		return $_settings;	
	}
	
	function mp_woocommerce_price_tab() {
	 
		add_submenu_page(
			'woocommerce',
			__( 'Mobiplus Price Display', 'mp-woocommerce-price' ),
			__( 'Mobiplus Price Display', 'mp-woocommerce-price' ) ,
			'manage_options' ,
			'admin.php?page=wc-settings&tab=mp-price-display'
		);
		
	}
	
	public function mp_load_language() {
		
		load_plugin_textdomain( 'mp-woocommerce-price', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
}

endif;

if ( ! function_exists( 'MPW' ) ) {

	function MPW() {
		return new Mobiplus_Woocommerce_Display_Price;
	}
}

add_action("woocommerce_loaded", "MPW");